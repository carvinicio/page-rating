require('dotenv').config()
const moment = require('moment');
const { Client } = require('pg');
const express = require('express');
const app = express();
const { parse } = require("csv-parse");
const fs = require('fs');
const { PrismaClient } = require('@prisma/client')

const prisma = new PrismaClient()
const port = process.env.PORT || 3000;

app.use(express.json());

app.get('/ratings', async (req, res) => {
	var options = {
		where: {},
		orderBy: [
			{
				dateVoted: 'asc',
			}
		],
	}

	for (const [key, value] of Object.entries(req.query)) {
		if (key === 'dateVoted') {
			if (value.includes('|')) {
				let range = value.split('|');
				options.where.dateVoted = {
					gte: range[0],
					lt: range[1]
				}
			} else {
				options.where.dateVoted = {
					equals: value
				}
			}
		} else if (key === 'helpful') {
			options.where.helpful = {
				equals: value
			}
		} else {
			options.where[key] = { contains: value, mode: 'insensitive' };
		}
	}

	res.send(await prisma.ratings.findMany(options));
});

app.post('/ratings', async (req, res) => {
	let { ipAddress, pageUrl, helpful, pageTitle, product, comment, email, indicators } = req.body;

	let response = await prisma.ratings.create({
		data: {
			dateVoted: moment().format(),
			ipAddress: ipAddress,
			pageUrl: pageUrl,
			helpful: helpful,
			pageTitle: pageTitle,
			product: product,
			comment: comment,
			email: email,
			indicators: indicators
		}
	});

	res.send('success');
});

app.get('/ratings/:id', async (req, res) => {
	var options = {
		where: {
			id: {
				equals: req.params.id
			}
		},
		orderBy: [
			{
				dateVoted: 'asc',
			}
		],
	}

	res.send(await prisma.ratings.findMany(options));
});

// app.get('/old', async (req, res) => {
// 	const parser = parse({ delimiter: ',' }, function (err, data) {
// 		const client = new Client({
// 			connectionString: process.env.DATABASE_URL,
// 			ssl: {
// 				rejectUnauthorized: false
// 			}
// 		});

// 		let headers = data.shift();

// 		var query = 'INSERT INTO ratings ("' + headers[0] + '", "' + headers[1] + '", "' + headers[2] + '", "' + headers[3] + '", "' + headers[4] + '", "' + headers[5] + '", "' + headers[6] + '", "' + headers[7] + '", "' + headers[8] + '") VALUES ';

// 		for (let row of data) {
// 			var segment = ' (\'' + row[0] + '\', \'' + row[1] + '\', \'' + row[2] + '\', \'' + row[3] + '\', \'' + row[4] + '\', \'' + row[5] + '\', \'' + row[6] + '\', \'' + row[7].split('\'').join('\'\'') + '\', \'' + row[8] + '\'),';
// 			query = query.concat(segment);
// 		}

// 		query = query.slice(0, -1);
// 		query = query.concat(';');

// 		client.connect();
// 		client.query(query, (error, response) => {
// 			if (error) throw error;
// 			client.end();
// 			res.send(response.rows);
// 		});

// 	});
// 	fs.createReadStream('./oldData.csv').pipe(parser);
// });

app.listen(port, async () => {
	console.log(`Example app listening at http://localhost:${port}`);
});
