# Page Rating

Simple API for rating pages (good, bad).

## Setting Up

Run `npm install`.

heroku pg:psql -f ./start.sql -a page-rating

And run the application with `node app.js`.

## Endpoints

### GET `https://page-rating.herokuapp.com/ratings`

Returns all ratings from the DB, optionally you can set filters by using the query params = dateVoted, ipAddress, pageUrl, helpful, product, email.
PS dateVoted can be just the date or a range divided by `|`

### POST `https://page-rating.herokuapp.com/ratings`

Send a request with payload (JSON) with the next values = ipAddress, pageUrl, helpful, pageTitle, product, comment, email.

### GET `https://page-rating.herokuapp.com/ratings/{id}`

Return the rating associated with the id value.
