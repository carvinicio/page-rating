const { google } = require('googleapis');
const sheets = google.sheets('v4');

const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];

async function getAuthToken() {
  const auth = new google.auth.GoogleAuth({
    scopes: SCOPES
  });
  const authToken = await auth.getClient();
  return authToken;
}

async function getSpreadSheet({spreadsheetId, auth}) {
  const res = await sheets.spreadsheets.get({
    spreadsheetId,
    auth,
  });
  return res.data;
}

async function getSpreadSheetValues({spreadsheetId, auth, sheetName}) {
  const res = await sheets.spreadsheets.values.get({
    spreadsheetId,
    auth,
    range: sheetName
  });
  return res.data;
}

async function appendSpreadSheetValues({spreadsheetId, auth, values}) {
  const request = {
		spreadsheetId: spreadsheetId,
		range: 'A1:I1',
		valueInputOption: 'USER_ENTERED',
		resource: {
			values
		},
		auth: auth,
	};
  return (await google.sheets('v4').spreadsheets.values.append(request)).data;
}

module.exports = {
  getAuthToken,
  getSpreadSheet,
  getSpreadSheetValues,
  appendSpreadSheetValues
}